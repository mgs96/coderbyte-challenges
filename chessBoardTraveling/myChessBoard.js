function ChessboardTraveling(str) { 
  let board = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0]
  ];
  let inputParser = /[0-9] [0-9]/g;
  let results = str.match(inputParser);
  let initial = results[0].split(' ');
  let final = results[1].split(' ');

  board[parseInt(initial[0]) - 1][parseInt(initial[1]) - 1] = 1;
  board[parseInt(final[0]) - 1][parseInt(final[1]) - 1] = 2;

  let n = paths(board, parseInt(initial[0]) - 1, parseInt(initial[1]) - 1);
  return n;
}

function paths(board, i, j) {
  if(board[i][j] === 2) {
    return 1;
  } else {
    if(i === 8 || j === 8) {
      return 0
    } else {
      return paths(board, i + 1, j) + paths(board, i, j + 1);
    }
  }
}

console.log(ChessboardTraveling("(1 1)(8 8)"))