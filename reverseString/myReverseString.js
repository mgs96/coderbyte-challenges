function FirstReverse(str) { 
  let arr = str.split('');
  for (let i = 0; i < str.length / 2; i++) {
    let swap = arr[i];
    arr[i] = arr[str.length - i];
    arr[str.length - i] = swap;
  }
  return arr.join('');
}

// keep this function call here 
console.log(FirstReverse("I Love Code"));                            
