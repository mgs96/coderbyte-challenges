function FirstFactorial(num) { 

  // code goes here  
  let fact  = 1;
  for (let i = 1; i <= num; i++) {
      fact *= i;
  }
  return fact; 
         
}
   
// keep this function call here 
FirstFactorial(8);