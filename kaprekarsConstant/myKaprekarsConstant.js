function KaprekarsConstant(num) {
  let runs = 0;
  let newNum = num;
  while(newNum != 6174) {
    let stringNumber = newNum.toString();
    while (stringNumber.length < 4) 
      stringNumber += '0';
    let ascending = parseInt(stringNumber.split('').sort((x, y) => x.localeCompare(y)).join(''));
    let descending = parseInt(stringNumber.split('').sort((x, y) => y.localeCompare(x)).join(''));
    newNum = ascending > descending ? ascending - descending : descending - ascending;
    runs++
  }
  return runs;
}

console.log(KaprekarsConstant(9831));