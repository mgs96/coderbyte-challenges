function EightQueens(strArr) {
  let board = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0]
  ];
  
  for (const pair of strArr) {
    board[parseInt(pair[1]) - 1][parseInt(pair[3]) - 1] = 1;
  }

  console.log(board);

  // let n = paths(board, parseInt(initial[0]) - 1, parseInt(initial[1]) - 1);
  // return n;
}

function paths(board, i, j) {
  if(board[i][j] === 2) {
    return 1;
  } else {
    if(i === 8 || j === 8) {
      return 0
    } else {
      return paths(board, i + 1, j) + paths(board, i, j + 1);
    }
  }
}

console.log(EightQueens(["(2,1)", "(4,3)", "(6,3)", "(8,4)", "(3,4)", "(1,6)", "(7,7)", "(5,8)"]))