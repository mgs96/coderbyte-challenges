function SimpleSymbols(str) { 
  let letterRegex = /[a-z]/;
  let arr = str.split('');
  let valid = false;
  for (let i = 0; i < arr.length; i++) {
    if (letterRegex.test(arr[i])) {
      if (i == 0 || i == arr.length - 1) {
        return false;
      } else {
        if (arr[i - 1] == '+' && arr [i + 1] == '+') {
          valid = true;
        } else {
          return false;
        }
      }
    }
  }
  return valid;         
}
   
// keep this function call here 
console.log(SimpleSymbols("f++d+"));
