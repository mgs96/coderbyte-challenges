function LetterChanges(str) { 
  let alph = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  let letterRegex = /[a-z]/;
  let vowelRegex = /[aeiou]/;
  let wordArray = str.split('');
  let newWord = [];
  for (let letter = 0; letter < wordArray.length; letter++) {
    newWord[letter] = letterRegex.test(wordArray[letter]) ? alph[alph.indexOf(wordArray[letter]) + 1] : wordArray[letter];
    newWord[letter] = vowelRegex.test(newWord[letter]) ? newWord[letter].toUpperCase() : newWord[letter];
  }
  return newWord.join(''); 
         
}
   
// keep this function call here 
console.log(LetterChanges("a confusing /:sentence:/[ this is not!!!!!!!~")); 


  