function LetterCapitalize(str) { 
  let arr = str.split('');
  return arr.map((letter, index, arr) => (index === 0) || (arr[index - 1] == " ") ? letter.toUpperCase() : letter).join('');       
}
   
// keep this function call here 
console.log(LetterCapitalize("i ran there"));