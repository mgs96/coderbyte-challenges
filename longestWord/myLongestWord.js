function LongestWord(sen) { 

  // code goes here  
  sen = sen + " ";
  let longestWord = '';
  let word = ''
  let letterStreak = 0;
  let letterRegex = /[A-Za-z]/;
  let maxStreak = 0;
  for (const letter of sen) {
      if (letterRegex.test(letter)) {
          letterStreak += 1;
          word += letter;
          
      } else {
          if (letterStreak > maxStreak) {
              maxStreak = letterStreak;
              longestWord = word;
          }
          word = '';
          letterStreak = 0;
      }
  }
  return longestWord; 
         
}
   
// keep this function call here 
console.log(LongestWord("fun&!! time"));